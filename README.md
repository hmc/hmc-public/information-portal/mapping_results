# Mapping Results

Repository, for exchange and accumulation of mapped information by the individual hubs during the CCT1 mapping efforts.
## Folder Layout:

1. `hmc_wide` : here all results are collected, and only these are checked and validated with each commit through CI runs, the hubs_dumps are not, thats why they are called dumps.
under `links` put all `relations` between categories/entities as json files
under `nodes` put all `entities/categories` here. These are nodes in the graph
under `tables` you find the same content as in the other folders only as csv tables

From this format is is very easy to throw this data (or a subset of it) into any front end or evaluation tool.
(We maybe want to collect some examples and look at the RDM-info pool case)

2. `hubs_dumps` here you can just put data in any format what so ever, these files are NOT checked and NOT validated, if you don't do it yourself.
But at least we then see that something is there and has to be `ported` or merged into the pool

3. `util` this folder contains some python scripts and functions to write jsons and tables

4. `tests` this folder contains integration tests, which will be run with ever commit. You can of course run them locally before you commit something ;-).
## How to map:

Options:
1. direct json input (if per hand helpful for that https://rjsf-team.github.io/react-jsonschema-form/)
2. input in tables, i.e .csv or .xls table which is schema conform, then use the scripts to extract the json files and update the combined tables with the scripts

if you have ideas to improve this, or other workflows, open a discussion about it (like and issue, or on mattermost, or cct1 meeting).

Additional keys are possible:
if you want to store some additonal information which is not (yet) CCT1 schema conform, you can do so by adding this information with 
keys prefixed with an `_`. All keys starting with an `_` will be ignored prior validation. This is useful to see over time if changes and which changes to the CCT1 schema might make sense. Also this may be useful to store information which is HMC specific and useful to evaluate but has not to do with the object itself (for example how much many people within HGF use a certain Repo).

## Helpful mapping tools

- json input per hand:
https://rjsf-team.github.io/react-jsonschema-form/

python:
1. jsonschema
2. json
3. some things can be prefilled, see the data_mining package

- libreoffice, or excel if you map in tables.
## How to find what is already there

1. Local (terminal) use of `grep` in the `nodes` or `links` folders ([tutorial](https://ostechnix.com/the-grep-command-tutorial-with-examples-for-beginners/))
2. Use gitlab search bar (as far as I know one cannot serach subfolders seperately, only the full repo)
3. Open tables files locally and search in there, or filter

Notice. So far we do not have an automated dublicate check, or a similarity check.
Overall, dublicates are not a problem. We can solve that later on, but of course we want to avoid double work.

## Integration tests

To check the consistency of your input you can run the integration tests (you need pytest to run it)

```
cd tests
pytest
```

If a test fails it will report what is not validating at what not

## Installation

For most use cases you do not need to install this, i.e one can run stuff without installation from the util folder, at least if one hast the category-definitions installed.

```
git clone
cd  mapping_results
pip install .
```

Notice that you need https://gitlab.hzdr.de/hmc/hmc/cct-1-mapping/category-definitions  for the schema files.
The installation of mapping results will work if you have setup git ssh with the gitlab instance, since it is an hmc internal repo and not public.
Otherwise first install category-definitions.
## Comments:

Currently we store the data twice, if everybody is knows how to use the provided scripts to generate the tables, we could remove the tables from the repo and
avoid this data dublication.
