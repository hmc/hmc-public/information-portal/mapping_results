import os
import json
import datetime
import jsonschema
import pytest
from hmc_schemas import SCHEMAREGISTRY
from hmc_schemas import load_schema #, validate_with
from jsonschema import validate, exceptions

from jsonschema.exceptions import ValidationError, SchemaError
import os
from pathlib import Path
# cwd = os.getcwd()
# print(cwd)
# os.chdir('D:\\HMC\\code\\category-definations\\category-definitions')
# cwd = os.getcwd()
# print(cwd)



TEST_DIR = os.path.dirname(os.path.abspath(__file__))

# This auto discovers files, but only named test.json and within the supported folder structure
schemas_data = []
for key, schemas in SCHEMAREGISTRY.items():
    for schema in schemas:
        version = schema[1]
        version = version.replace('.', '_')
        # for now we assume one test file per version
        testdata_path = os.path.abspath(os.path.join(TEST_DIR, f'./data/{key}/{version}/test.json'))
        if os.path.exists(testdata_path):
            schemas_data.append(((key, schema[1]), testdata_path))
        else:
            print(testdata_path)

print(schemas_data)
# schemas_data = [(('metadata_standard', '1.0.0'), './schema_registry.json')]


@pytest.mark.parametrize('schema_data', schemas_data)
def test_data_validation_tests(schema_data):
    """
    Load a given schema and tries to validate a given full test data instance

    schema_data : ((category, version), datainstance_file_path)
    Throws a jsonschema.exceptions.SchemaError if schema is invalid
    or a jsonschema.exceptions.ValidationError if the validation is not sucessfull
    """
    try:
        print(schema_data)
        schemad = schema_data[0]
        datafilepath = schema_data[1]

        with open(datafilepath, 'r', encoding='utf-8') as fileo:
            jsondict = json.load(fileo)

        # get right schema
        schema = load_schema(schemad[0], version=schemad[1])
        # print(schema)
        # ignore all keys with '_' for validation
        datat = {}
        for key, val in jsondict.items():
            if not key.startswith('_'):
                datat[key] = val
        # try:
        # validate(instance=datat, schema=schema)  # Will throw errors if not
        # validate_with(datat, category=schema[0], version=schema[1])
        ############################ iter_errors ##################################
        validator = jsonschema.Draft7Validator(schema)
        errList = validator.iter_errors(datat)
        for err in errList:
            err = str(err).replace("\n", " ")
            print(err)
            logValidationExceptions("Validation Error", str(err), schemad[0], schemad[1], datafilepath, "")
            # err = str(err).split("\n")
            # completeMsg = ''
            # for i in range(len(err)):
            #     if err[0]:
            #         completeMsg = completeMsg+err[0]
        ############################################################################
        print("no exception occurred!")

        # except exceptions.SchemaError as e:
        #     print(f"Schema Error: {e.message}")
        #     logValidationExceptions("Schema Error", e.message, schemad[0], schemad[1], datafilepath, e.args)  # data_hub
        #
        # except exceptions.ValidationError as e:
        #     print(f"Validation Error: {e.message}")
        #     logValidationExceptions("Validation Error", e.message, schemad[0], schemad[1], datafilepath, e.path[0])
        #
        # except json.decoder.JSONDecodeError as e:
        #     print(f"JSON Decode Error: {e}")
        #     logValidationExceptions("JSON Decode Error", e.message, schemad[0], schemad[1], datafilepath)
        #
        # except exceptions.RefResolutionError as e:
        #     print(f"Reference Error: {e}")
        #     logValidationExceptions("Reference Error", str(e), schemad[0], schemad[1], datafilepath,"")

        # except CustomValidationException as e:
        #     print(f"Custom Validation Error: {e}")
        # except Exception as e:
        #     print("General exception:", str(e))
        #     logValidationExceptions("Reference Error", str(e), schemad[0], schemad[1], datafilepath,"")

    except json.decoder.JSONDecodeError as e:
        print(f"JSON Decode Error: {e}")
        logValidationExceptions("JSON Decode Error", e.args[0], schemad[0], schemad[1], datafilepath, e.args[0]) # e.schema["type"]
        # print("Exception caught before validation:", str(e.args[0]))
    except OSError as e:
        print("General exception:", str(e))
        logValidationExceptions("File exception:", str(e), schemad[0], schemad[1], datafilepath, "")
    except Exception as e:
        print("General exception:", str(e))
        logValidationExceptions("General exception:", str(e), schemad[0], schemad[1], datafilepath,"")


def logValidationExceptions(errType, message, schematype, schemaVersion, datafilepath, lineNo):
    # print("Writing to a file")
    try:
        with open('validation_issues.json', 'r') as valfile:
            valObjs = json.load(valfile)

        data = {"Document Type": schematype, "Schema Version": schemaVersion, "Document": datafilepath, "Exception Type": errType, "Exception Message": message, "Property or Line no.": lineNo, "Timestamp": datetime.datetime.now()}
        valObjs.append(data)
        with open('validation_issues.json', 'w') as valfile2:
            json.dump(valObjs, valfile2, indent=2, default=str)  # 'default' property converts any non-serializable entity into (here) a str
        # print("file write completed!")
    except Exception as e:
        print("caught in function", str(e))