# Util documentation

## `validate_mapping_result.py`

Quick and dirty script for mapping result validation against the defined schemas.

1. [Install category definitions](https://gitlab.hzdr.de/hmc/hmc/cct-1-mapping/category-definitions#installation) (schemas) first.
2. Edit `validate_mapping_result.py` accordingly:
  - Specify the folder of mapping results you want to validate in `results_folder`.
  - Specify the actual mapping result which is to be validates in `result_file`.
  - Specify the schema which is to be used for validation in `schema`, which can be one of:
    - `data_format`
    - `data_source`
    - `dataset`
    - `document`
    - `license`
    - `links`
    - ... check [available schemas](https://gitlab.hzdr.de/hmc/hmc/cct-1-mapping/category-definitions/-/tree/master/hmc_schemas) accordingly.
3. Run the script:
   ```
   python ./validate_mapping_result.py
   ```
4. In case of errors check the issue tracker because the schemas are adjusted constantly and may break validation.

Todo:
- [ ] convert `validate_mapping_result.py` into a comfortable cmd script being callable with command line parameters in order to omit editing the file for each validation beforehand.