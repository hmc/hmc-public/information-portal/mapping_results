#!/bin/env python3
# -*- coding: utf-8 -*-

"""
Script to update all tables from the json files present in hmc_wide

"""
from json_util import write_all_jsons_to_csv
import os

def update_all_tables():
    """Updates all tables under hmc_wide/tables by the json files present.

    THIS OVERWRITES ALL EXISTING TABLES!

    if you have changed a table, please first export that table to json files
    """
    categories = ['organization', 'data_format', 'software', 'pid_type', 'data_source', 'metadata_standard', 'terminology']
    PACKAGE_DIRECTORY = os.path.dirname(os.path.abspath(__file__))

    default_tablefile = str(os.path.join(PACKAGE_DIRECTORY, '../hmc_wide/tables/all_nodes_table.csv'))
    default_tablefile_links = str(os.path.join(PACKAGE_DIRECTORY, '../hmc_wide/tables/all_links_table.csv'))
    default_folder_links = str(os.path.join(PACKAGE_DIRECTORY, '../hmc_wide/links/'))

    default_folder = str(os.path.join(PACKAGE_DIRECTORY, '../hmc_wide/nodes/'))
    default_folder_table = str(os.path.join(PACKAGE_DIRECTORY, '../hmc_wide/tables/'))


    for cat in categories:
        print(f'Updateing {cat}')
        tablename = f'hmc_{cat}_table.csv'
        tablefilename = os.path.join(default_folder_table, tablename)
        write_all_jsons_to_csv(tablefilename=tablefilename, filelist=None, filefolder=default_folder, filter=cat, overwrite=True)

    print('Updating node table')
    write_all_jsons_to_csv(tablefilename=default_tablefile, filelist=None, filefolder=default_folder, filter=None, overwrite=True)

    print('Updating link table')
    write_all_jsons_to_csv(tablefilename=default_tablefile_links, filelist=None, filefolder=default_folder_links, filter=None, overwrite=True)

if __name__== '__main__':
    update_all_tables()
