import React, { Component } from "react";
import PropTypes from 'prop-types';
import { InputGroup, Form, Button } from 'react-bootstrap'

const refPattern = /{"\$ref": ?"[a-zA-Z0-9_]+\.json"}/;
const treeApi = "https://gitlab.hzdr.de/api/v4/projects/3009/repository/tree?path=hmc_schemas&per_page=999"
const fileApi = "https://gitlab.hzdr.de/api/v4/projects/3009/repository/files/hmc_schemas%2F"

function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response.json();
}

class Gitlab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pat: "",
      files: [],
      file: "",
      invalidPat: false
    };
    this.handleChangePAT = this.handleChangePAT.bind(this);
    this.handleChangeSchema = this.handleChangeSchema.bind(this);
    this.fetchFiles = this.fetchFiles.bind(this);
  }

  handleChangePAT(event) {
    this.setState({ pat: event.target.value });
  }

  handleChangeSchema(event) {
    this.setState({ file: event.target.value });
    if (event.target.value.endsWith(event.target.value)) {
      this.fetchSchema(event.target.value)
        .then(schema => {
          this.props.onSchemaChange(schema)
        })
    }
  }

  fetchFiles() {
    fetch(treeApi, {
      method: "GET",
      headers: {
        'accept': 'application/json',
        'Private-Token': this.state.pat
      }
    })
      .then(response => handleErrors(response))
      .then((data) => {
        this.setState({ files: data, invalidPat: false })
      }).catch((error) => {
        console.log(error);
        this.setState({ invalidPat: true })
      });
  }

  async fetchSchema(name) {
    const response = await fetch(fileApi + name + "/raw", {
      method: "GET",
      headers: {
        'accept': 'application/json',
        'Private-Token': this.state.pat
      }
    })
    const schema = await response.json();
    let strRep = JSON.stringify(schema);

    const matched = strRep.match(refPattern);
    if (matched) {
      for (let i = 0; i < matched.length; i++) {
        const splitted = matched[i].split('"');
        const filenameToLoad = splitted[splitted.length - 2];
        const fetched = await this.fetchSchema(filenameToLoad);
        strRep = strRep.replace(matched[i], JSON.stringify(fetched));
      }
    }

    let result = {};
    try {
      result = JSON.parse(strRep);
    } catch (e) {
      return {};
    }

    // TODO: https://gitlab.hzdr.de/hmc/hmc/cct-1-mapping/category-definitions/-/issues/26
    delete result.propertyNames;
    return result;
  }

  handleLogin(event) {
    this.fetchFiles();
    event.preventDefault();
  }

  render() {
    return (
      <div className="Gitlab">

        <InputGroup className="mb-3 mt-3">
          <Form.Control
            placeholder="Personal Access Token"
            type="password"
            value={this.state.value}
            onChange={this.handleChangePAT}
            isInvalid={this.state.invalidPat}
          />
          <Button variant="primary" onClick={this.fetchFiles}>
            Fetch
          </Button>
        </InputGroup>

        <Form.Control
          as="select"
          value={this.state.Schema}
          onChange={this.handleChangeSchema}
        >{
            this.state.files.map(file =>
              <option key={file.id}>{file.name}</option>)
          }</Form.Control>

      </div >
    );
  }
}

Gitlab.propTypes = {
  onSchemaChange: PropTypes.func
};

export default Gitlab;
