import "./App.css";
import React, { Component } from "react";
import Gitlab from "./components/Gitlab"
import Form from "@rjsf/bootstrap-4";
import { Button } from 'react-bootstrap'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      schema: {},
      data: {}
    };
    this.handleSchemaChange = this.handleSchemaChange.bind(this);
    this.handleDataChange = this.handleDataChange.bind(this);
    this.handleError = this.handleError.bind(this);
    this.copyData = this.copyData.bind(this);
  }

  handleSchemaChange(schema) {
    this.setState({ schema: schema })
  }

  handleDataChange({ formData }) {
    this.setState({ data: formData })
  }

  handleError(e) {
    console.log(e)
  }

  copyData() {
    console.log(this.state.data)
    navigator.clipboard.writeText(JSON.stringify(this.state.data, null, 2)).then(function () {
      alert('Copying to clipboard was successful!');
    }, function (err) {
      alert('Could not copy text: ', err);
    });
  }

  render() {
    return (
      <div className="App">
        <Gitlab onSchemaChange={this.handleSchemaChange} />
        <Form
          schema={this.state.schema}
          formData={this.state.data}
          onChange={this.handleDataChange}
          onError={this.handleError}
          onSubmit={this.copyData}
          omitExtraData
          liveValidate
        >
          <Button type="submit"> Copy JSON </Button>
        </Form>
      </div>
    );
  }
}

export default App;
