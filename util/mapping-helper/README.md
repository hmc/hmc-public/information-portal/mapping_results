# Mapping Helper

> Contributions are welcome :muscle:

![screenshot](doc/images/Screenshot.png "Screenshot")

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
It heavily utilizes [react-jsonschema-form](https://github.com/rjsf-team/react-jsonschema-form) and [react-bootstrap](https://react-bootstrap-v3.netlify.app/).

## Getting started

### Run the app locally

- In order to run this project you need to have nodejs and npm installed.
- Run `npm install` and `npm start` to run this project.
- Visit [http://localhost:3000](http://localhost:3000).

### Usage

- Create a [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with `api` permissions.
- Paste your Personal Access Token and press `Fetch` to fetch available schemas.
- Select a schema and fill in the form.
- When you are done filling, click `Copy JSON` to copy the created JSON to the clipboard.

### ToDo

- At the moment there is an issue with `propertyNames` (<https://gitlab.hzdr.de/hmc/hmc/cct-1-mapping/category-definitions/-/issues/26>).
- Improve the user experience by enabling and disabling the `Copy JSON` button and displaying alerts.
- Create merge requests automatically on submit.

## Development

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
