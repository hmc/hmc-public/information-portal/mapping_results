import os
import shutil
import json
import uuid
from jsonschema import validate
from jsonschema.exceptions import ValidationError
from hmc_schemas import load_schema
from pathlib import Path
# Collect the node files
FILE_PATH1 = '../hubs_dumps/'
hubs = ['hub_ast',  'hub_ee',  'hub_energy',  'hub_health',  'hub_info',  'hub_matter']


this_file = os.path.dirname(os.path.abspath(__file__))
nodefilefolder = os.path.abspath(os.path.join(this_file, FILE_PATH1))

nodejsonfilelist = []
nodes_uuids = []
for subdir, dirs, files in os.walk(nodefilefolder):
    for file_ in files:
        if file_.endswith('.json'):
            nodejsonfilelist.append(os.path.join(subdir, file_))
            nodes_uuids.append(file_.split('.json')[0])

folder_structure = {
        '10_policies': 'policy',
        '1_repositories' : 'repository',
        '5_data_sources' : 'data_source',
        '9_organizations' : 'organization',
        '11_documents': 'documents',
        '2_metadata_standards': 'metadata_standard',
        '6_identifiers' : 'pid_system',
        '12_schema_crosswalks' : 'crosswalk',
        '3_terminologies' : 'terminology',
        '7_data_formats' : 'data_format',
        '13_methods' : 'method',
        '4_software' : 'software',
        '8_licenses' : 'license'
        }

cats = list(folder_structure.keys())
cats.sort()
# maybe change this to get a test for each category

error_stats = {cat:{} for cat in folder_structure.values()}

# Since the file system does not allow for two files to have the same name,
# we do not have to check uniqueness. But it could be that someone overrides and
# existing file with something else. This has to be caught in the link check

def validate_nodefile(nodefile):
    """
    Test if all nodes json files within the nodefile list validate
    with the corresponding CCT1 schema
    """

    # read
    with open(nodefile, 'r', encoding='utf-8') as fileo:
        data = json.load(fileo)

    # find out type
    # the type can be found throuch the folder structure
    cat_type = data.get('_type', None)
    if cat_type is None:
        for key, val in folder_structure.items():
            if key in nodefile:
                cat_type = val
    if cat_type is None:
        id_ = data.get('_id', None)
        if id_ is None:
            raise ValueError('Could not find out Category of the node, please specify in "_type" key and or structure of "_id" key.')
        #nodefile.replace('.','/')
        #assert id_ == nodefile
        cat_type = id_.split('/')[0].split('_')[-1]

    # get right schema
    schema = load_schema(cat_type)
    if schema is None:
        print(cat_type)
        raise ValueError(f'Schema not found for: {cat_type}')
    # ignore all keys with '_' for validation
    datat = {}
    for key, val in data.items():
        if not key.startswith('_'):
            datat[key] = val

    # validate, but catch exception to add to statistics
    try:
       validate(instance=datat, schema=schema) # Will throw errors if not
    except Exception as ex:
        # count and reraise
        stat = error_stats[cat_type].get(ex, 0)
        stat = stat + 1
        error_stats[cat_type][ex] = stat
        raise ex

for nodefile in nodejsonfilelist:
    try:
        validate_nodefile(nodefile)
    except (ValidationError, ValueError) as er:
        continue

    destfilename = nodefile.replace('/hubs_dumps/', '/hmc_wide/')
    creator_hub = []
    for hub in hubs:
        if hub in destfilename:
            creator_hub.append(hub)
            destfilename = destfilename.replace(hub, '')
    destfilename = destfilename.replace('//', '/')
    destfilename = Path(destfilename)
    old_filename = destfilename.name
    gen_uuid = uuid.uuid4()
    destfilename = destfilename.with_name(str(gen_uuid) + '.json')
    print(destfilename)
    # move file from dumbs to hmc_wide if it validates
    if os.path.isfile(destfilename):
        print(f'File {destfilename} already present, I do not move it please merge by hand.')
        continue
    # add creator hub to json
    with open(nodefile, 'r') as fileo:
        data = json.load(fileo)

    chub = data.get('_hub', [])
    if isinstance(chub, list):
        for hub in chub:
            creator_hub.append(hub)
    if isinstance(chub, str):
        creator_hub.append(chub)
    data['_hub'] = list(set(creator_hub))
    data['_id'] = str(gen_uuid)
    data['_old_filename'] = old_filename
    if len(list(set(creator_hub))) >1:
        print(creator_hub)
    with open(nodefile, 'w') as fileo:
        json.dump(data, fileo, indent=4, separators=(", ", ": "), sort_keys=True)
    # create a link file for the respected hub in the future this will also be in record metadata
    shutil.move(nodefile, destfilename)
    # also create link


