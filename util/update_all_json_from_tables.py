#!/bin/env python3
# -*- coding: utf-8 -*-

"""
Script to update all tables from the json files present in hmc_wide

"""
from json_util import write_csv_to_jsonfiles
import os

def update_all_jsons_from_tables():
    """Updates all tables under hmc_wide/tables by the json files present.

    This does NOT override existing entries, therefore if you have modified an entry in the
    """
    categories = ['organization', 'data_format', 'software', 'pid_type', 'data_source', 'metadata_standard', 'terminology']
    PACKAGE_DIRECTORY = os.path.dirname(os.path.abspath(__file__))

    default_tablefile = str(os.path.join(PACKAGE_DIRECTORY, '../hmc_wide/tables/all_nodes_table.csv'))
    default_tablefile_links = str(os.path.join(PACKAGE_DIRECTORY, '../hmc_wide/tables/all_links_table.csv'))
    default_folder_links = str(os.path.join(PACKAGE_DIRECTORY, '../hmc_wide/links/'))

    default_folder = str(os.path.join(PACKAGE_DIRECTORY, '../hmc_wide/nodes/'))
    default_folder_table = str(os.path.join(PACKAGE_DIRECTORY, '../hmc_wide/tables/'))

    id_key = '_id'
    for cat in categories:
        print(f'Updateing jsons for {cat}')
        tablename = f'hmc_{cat}_table.csv'
        tablefilename = os.path.join(default_folder_table, tablename)
        if os.path.exists(tablefilename):
            write_csv_to_jsonfiles(tablefilename=tablefilename, id_key=id_key, dest_folder=default_folder, overwrite=False)

    print('Updating node jsons')
    write_csv_to_jsonfiles(tablefilename=default_tablefile, id_key=id_key, dest_folder=default_folder, overwrite=False)

    print('Updating link jsons')
    write_csv_to_jsonfiles(tablefilename=default_tablefile_links, id_key=id_key, dest_folder=default_folder_links, overwrite=False)

if __name__== '__main__':
    update_all_jsons_from_tables()
