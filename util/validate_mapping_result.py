from pathlib import Path
import json
from hmc_schemas import validate_with

results_folder = Path("D:/WS_HMC/mapping_results/hubs_dumps/hub_ast/data_formats")
result_file = results_folder / "railML.json"
schema = "data_format"
with open(result_file, 'r', encoding='utf-8') as fileo:
        result = json.load(fileo)

validate_with(result, schema)
