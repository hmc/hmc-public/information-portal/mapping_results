#!/bin/env python3
# -*- coding: utf-8 -*-

"""
Script to update the nodes under from hmc_wide/tables tables, or any table given

"""
import argparse
from json_util import write_csv_to_jsonfiles
import os



#def write_csv_to_jsonfiles(tablefilename: str, id_key:str = 'id', dest_folder: str=None, dest_filenames: List[str]=None):
if __name__== '__main__':
    parser = argparse.ArgumentParser(description = 'execute write_csv_to_jsonfiles')
    parser.add_argument('tablefilename', help='your name, enter it')
    parser.add_argument('id_key', help='Id key in the table, default "id"')
    parser.add_argument('dest_folder', help='folder to generate the files to')

    args = parser.parse_args()
    PACKAGE_DIRECTORY = os.path.dirname(os.path.abspath(__file__))

    default_tablefile = os.path.join(PACKAGE_DIRECTORY, 'hmc_wide/tables/all_nodes_table.csv')
    default_folder = os.path.join(PACKAGE_DIRECTORY, 'hmc_wide/nodes')

    if args.tablefilename is None:
        tablefilename = default_tablefile
    if args.id_key is None:
        id_key = 'id'
    if args.dest_folder is None:
        dest_folder = default_folder

    write_csv_to_jsonfiles(tablefilename=tablefilename, id_key=id_key, dest_folder=dest_folder)
