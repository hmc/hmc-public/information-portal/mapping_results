#!/bin/env python3
# -*- coding: utf-8 -*-

"""
Script to update the nodes under from hmc_wide/tables tables, or any table given

"""
import argparse
from json_util import write_all_jsons_to_csv
import os


if __name__== '__main__':
    PACKAGE_DIRECTORY = os.path.dirname(os.path.abspath(__file__))

    default_tablefile = str(os.path.join(PACKAGE_DIRECTORY, '../hmc_wide/tables/all_nodes_table.csv'))
    default_folder = str(os.path.join(PACKAGE_DIRECTORY, '../hmc_wide/nodes/'))

    parser = argparse.ArgumentParser(description = 'execute write_all_jsons_to_csv')
    parser.add_argument('-out', '--tablefilename', help='Path to the table, enter it', type=str, default=default_tablefile)
    parser.add_argument('-fl', '--filelist', help='List of paths to files to read', default=None)
    parser.add_argument('-in', '--filefolder', help='folder read json files from', type=str, default=default_folder)
    parser.add_argument('-fil', '--filter', help='filename has to contain that', type=str, default=None)
    parser.add_argument('--overwrite', help='filename has to contain that', type=bool, default=False)
    args = parser.parse_args()
    #print(args.tablefilename)
    #print(args.filefolder)
    write_all_jsons_to_csv(tablefilename=args.tablefilename, filelist=args.filelist, filefolder=args.filefolder, filter=args.filter, overwrite=args.overwrite)
