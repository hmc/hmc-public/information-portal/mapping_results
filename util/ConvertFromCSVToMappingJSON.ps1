Param(
    $file = 'C:\...\software.csv',
    $targetFolder = 'C:\...\mapping data software'
)

$CSV = Import-Csv $File -Delimiter ";"

ForEach($entry in $CSV){
    $jsonObject = [ordered]@{}
    ForEach($property in $entry.PSObject.Properties){
        if(-not $property.Value){
            continue
        } 
        if($property.Name.Contains("_")){
            #1:Name of property,2:number of item;3:name of item property
            $splitName = $property.Name.Split("_")
            $collection = @()
            if($jsonObject[$splitName[0]]){
                $collection = $jsonObject[$splitName[0]]
            }
            
            if($splitName.Count -eq 2){
                $collection += $property.Value
            }else{
                if($collection.Count -gt [int]$splitName[1]-1){
                    $nestedObject = $collection[$splitName[1]-1]
                }else{
                    while($collection.Count -lt [int]$splitName[1]){
                        $collection += @{}
                    }

                    $nestedObject = $collection[$splitName[1]-1]
                }
                $nestedObject.Add($splitName[2], $property.Value) 
            }
            $jsonObject.Remove($splitName[0])
            $jsonObject.Add($splitName[0], $collection)
            
        }else{
            $jsonObject.Add($property.Name, $property.Value)
        }
        
    }
    $targetFile = $targetFolder + "/" + $jsonObject."resourceName"[0].name.Replace("/",",").Replace("|",",").Replace("\",",").Replace(":"," ").Replace("*","").Replace("?","").Replace("<","(").Replace(">",")").Replace("`"","") +".json"
    New-Item -ItemType Directory -Force -Path $targetFolder
    $jsonString = ConvertTo-Json -Depth 3 $jsonObject
    [IO.File]::WriteAllLines($targetFile, $jsonString)
 }
