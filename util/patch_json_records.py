import os
import json
#from data_mining.util.mapping_util import apply_jq_mapping, apply_jq_mapping_file
#from data_mining.util.mapping_util import apply_jq

nodefilefolder = './hmc_wide/'
#nodefilefolder = './4_software/'

nodejsonfilelist = []
nodes_uuids = []
for subdir, dirs, files in os.walk(nodefilefolder):
    for file_ in files:
        if file_.endswith('.json'):
            nodejsonfilelist.append(os.path.join(subdir, file_))
            nodes_uuids.append(file_.split('.json')[0])
### helpers
def correct_art(data_patched):
    """correct spelling of art"""
    fields = data_patched.get('helmholtzResearchField', None)
    new_f = []
    if fields is None:
        return data_patched

    for field in fields:
        if field == 'Aeronautics Space and Transport':
            field = 'Aeronautics, Space and Transport'
        new_f.append(field)
    data_patched['helmholtzResearchField'] = new_f
    return data_patched

typo_list = [['softwareType', 'data processing/analysis tool', 'data processing / analysis tool'],
        ['softwareType', 'experiment/device control software or interface', 'experiment / device control software or interface'],
        ['softwareType', 'simulation/modelling software', 'simulation / modelling software'],
        ['scientificDiscipline','Other', 'other'], ['formatType', 'Other', 'other'],
        ['scientificDiscipline', 'Earth Science: Atmospheric Science: Meterology', 'Earth Science: Atmospheric Science: Meteorology'],
        ['scientificDiscipline','Aeronautics Space and Transport', 'Aeronautics, Space and Transport'],
        ['helmholtzResearchField', 'Aeronautics Space and Transport', 'Aeronautics, Space and Transport'],
        ['terminologyType', '(controlled) vocabulary', 'controlled vocabulary'], ['terminologyLanguage', 'EN', 'en'],
        ['nameLanguage', 'en/de', 'en']
        ]

# fileNameExtension to filenameExtension

typo_key_list = [['fileNameExtension','filenameExtension'], ['LicenseScope', 'licenseScope']]

def fix_all_typos(data):
    """
    """
    for typo in typo_key_list:
        data = fix_typo_key(data, typo[0], typo[1])
    for typo in typo_list:
        data = fix_typo(data, typo[0], typo[1], typo[2])

    return data


def fix_typo_key(data, key, correct):
    fields = data_patched.get(key, None)
    if fields is None:
        return data_patched
    data_patched.pop(key)
    data_patched[correct] = fields
    return data_patched


def fix_typo(data, key, typo, correct):
    fields = data_patched.get(key, None)
    new_f = []
    if fields is None:
        return data_patched
    if isinstance(fields, str):
        if fields == typo:
            fields = correct
        new_f = fields
    else:
        for field in fields:
            if field == typo:
                field = correct
            new_f.append(field)
    data_patched[key] = new_f
    return data_patched



def patch_dataupload(data):
    """Remove none as data uploadtype"""
    data_patched = data
    dates = data_patched.get('dataUploadType', [])
    if dates is None:
        # remove or start date?
        print('dataUploadType')
    data_patched.pop('dataUploadType')
    return data_patched

def patch_true(data_patched):
    """Update true to True in all files"""

    for key, val in data_patched.items():
        if (val == 'true') or (val == 'True'):
            data_patched[key] = True
        if (val == 'false') or (val == 'False'):
            data_patched[key] = False
        if isinstance(val, dict):
            data_patched[key] = patch_true(val)
    return data_patched

keys_comment = ['Link to Organization', 'Link to Terminology',
                'Link to Data Format', 'Link to Metadata Standard',
                'Supported Metadata standart', 'Hersteller',
                'hersteller', 'Link to License', 'Link to Method', 'relatedSoftware',
                'relatedDataFormat', 'relatedOrganization', 'Link to Software', 'stanaarts']

def comment_out(data_patched):
    for key in keys_comment:
        comment =  data_patched.get(key, None)
        if comment is not None:
            data_patched[f'_{key}'] = comment
            data_patched.pop(key)
    return data_patched



def patch_comments(data_patched):


    comment =  data_patched.get('Comments', None)
    if comment is not None:
        data_patched['_comments'] = comment
        data_patched.pop('Comments')
    return data_patched


keys_array_patch = ['organizationType', 'formatType', 'keyword', 'databaseAccess', 'dataAccess', 'dataUploadType']
def patch_array(data_patched):
    """
    make all keys values arrays if they are not
    """
    for key in keys_array_patch:
        data_patched = patch_array_key(data_patched, key)

    return data_patched

def patch_array_key(data_patched, key):
    """Make value of key an array if not"""
    data = data_patched.get(key, None)
    if data is None:
       return data_patched

    if not isinstance(data, list):
        data_patched[key] = [data]

    return data_patched

keys_rearray_patch = ['isOpenStandard', 'dataFormatVersion','filenameExtension', 'terminologyType']
def patch_array_tostr(data_patched):
    """
    make all keys values arrays if they are not
    """
    for key in keys_rearray_patch:
        data_patched = patch_array_key_to_string(data_patched, key)

    return data_patched

def patch_array_key_to_string(data_patched, key):
    """Make value of key an array if not"""
    data = data_patched.get(key, None)
    if data is None:
       return data_patched

    if isinstance(data, list):
        #print('##### HERE ######')
        if len(data) == 1:
            print('##### HERE ######')
            data_patched[key] = data[0]

    return data_patched



def add_relation_type(data):

    new_centers = []
    centers = data.get('relatedHelmholtzCentre', [])
    for center in centers:
        rel = center.get('relationType', "uses")
        center['relationType'] = rel
        new_centers.append(center)
    return data

####



for jfile in nodejsonfilelist:
    jfile2 = jfile#.replace('.json', 'patched.json') #overwrite
    print(jfile)
    print(jfile2)
    with open(jfile, 'r') as fileo:
        data = json.load(fileo)
    data_patched = dict(data) #.copy()
    #data_patched = patch_dataupload(data)
    #data_patched = correct_art(data)
    #data_patched = patch_true(data)
    #data_patched = patch_comments(data_patched)
    data_patched = fix_all_typos(data_patched)
    #data_patched = comment_out(data_patched)
    data_patched = patch_array(data_patched)
    #data_patched = patch_array_tostr(data_patched)
    #data_patched = patch_true(data_patched)
    #data_patched = add_relation_type(data_patched)

    #if data_patched != data:
    print(data_patched)

    with open(jfile2, 'w') as fileo:
        json.dump(data_patched, fileo, indent=4, separators=(", ", ": "), sort_keys=True)



'''
# this will only work if these keys are there, otherwise it will contain null
patches = [r'.databaseAccess=.databaseAccess[0] | .dataUploadType=.dataUploadType[0] | .offersEnhancedPublication=.offersEnhancedPublication[0]']

"""
This one I do not how to jq it
Failed validating 'enum' in schema['allOf'][0]['properties']['date']['items']['properties']['dateType']:
    {'description': 'The type of date.',
     'enum': ['start date', 'end date', 'modified'],
     'type': 'string'}

On instance['date'][0]['dateType']:
    'other'

"""



for jfile in nodejsonfilelist:
    jfile2 = jfile#.replace('.json', 'patched.json') #overwrite
    print(jfile)
    print(jfile2)
    #apply_jq_mapping_file(jfile, jfile2, patch)
    with open(jfile, 'r') as fileo:
        data = json.load(fileo)

    data_patched = data
    for patch in patches:
        try:
            data_patched = apply_jq(data_patched, mapping=patch)
        except NameError as ex:
            print(ex)
            continue

    dates = data_patched.get('date', [])
    if len(dates)>=1:
        for i, date in enumerate(dates):
            if date['dateType'] == 'other':
                # remove or start date?
                print('fixing date')
                data_patched['date'][i]['dateType'] = 'start date'
    #print(data_patched)

    with open(jfile2, 'w') as fileo:
        json.dump(data_patched, fileo, indent=4, separators=(", ", ": "), sort_keys=True)
'''

