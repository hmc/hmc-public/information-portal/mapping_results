import os
import json
from data_mining.util.mapping_util import apply_jq_mapping, apply_jq_mapping_file
from data_mining.util.mapping_util import apply_jq

nodefilefolder = './1_repositories/'

nodejsonfilelist = []
nodes_uuids = []
for subdir, dirs, files in os.walk(nodefilefolder):
    for file_ in files:
        if file_.endswith('.json'):
            nodejsonfilelist.append(os.path.join(subdir, file_))
            nodes_uuids.append(file_.split('.json')[0])


# this will only work if these keys are there, otherwise it will contain null
patches = [r'.databaseAccess=.databaseAccess[0] | .dataUploadType=.dataUploadType[0] | .offersEnhancedPublication=.offersEnhancedPublication[0]']

"""
This one I do not how to jq it
Failed validating 'enum' in schema['allOf'][0]['properties']['date']['items']['properties']['dateType']:
    {'description': 'The type of date.',
     'enum': ['start date', 'end date', 'modified'],
     'type': 'string'}

On instance['date'][0]['dateType']:
    'other'

"""



for jfile in nodejsonfilelist:
    jfile2 = jfile#.replace('.json', 'patched.json') #overwrite
    print(jfile)
    print(jfile2)
    #apply_jq_mapping_file(jfile, jfile2, patch)
    with open(jfile, 'r') as fileo:
        data = json.load(fileo)

    data_patched = data
    for patch in patches:
        try:
            data_patched = apply_jq(data_patched, mapping=patch)
        except NameError as ex:
            print(ex)
            continue

    dates = data_patched.get('date', [])
    if len(dates)>=1:
        for i, date in enumerate(dates):
            if date['dateType'] == 'other':
                # remove or start date?
                print('fixing date')
                data_patched['date'][i]['dateType'] = 'start date'
    #print(data_patched)

    with open(jfile2, 'w') as fileo:
        json.dump(data_patched, fileo, indent=4, separators=(", ", ": "), sort_keys=True)


