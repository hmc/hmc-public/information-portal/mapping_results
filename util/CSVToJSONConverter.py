import json
import pandas as pd
from tkinter import filedialog as fd, ttk
import tkinter as tk

#iterates over every row of the .csv file and starts the conversion process.
def conversionManager(pathToCsv, pathOutput):
    i = 0
    csvImport = pd.read_csv(pathToCsv, ";")
    for index, row in csvImport.iterrows():
        row2 = row.to_dict()
        conversion(row2, pathOutput)
        i += 1


def conversion(csvFile, pathOutput):
    toDelete = []
    #bereitsEingetragen = []
    mainElement = dict()
    aktuelleListe = 0
    anzahlElemente = 0

#Data cleaning
    for key in csvFile:
        print(key)
        csvFile[key] = str(csvFile[key]).replace("\\", ",")
        csvFile[key] = str(csvFile[key]).replace("|", ",")
        csvFile[key] = str(csvFile[key]).replace("*", "")
        csvFile[key] = str(csvFile[key]).replace("?", "")
        csvFile[key] = str(csvFile[key]).replace("<", "(")
        csvFile[key] = str(csvFile[key]).replace(">", ")")
        csvFile[key] = str(csvFile[key]).replace("`", "")

        if pd.isna(str(csvFile[key])) or pd.isnull(str(csvFile[key])) or str(csvFile[key]) == "nan":
            toDelete.append(key)
    for k in toDelete:
        del csvFile[k]


    for key in csvFile:
        if str(key).__contains__("_"):
            print(key)
            splitter = []
            splitter = str(key).split("_")
            # print(splitter[0]) #resourceName
            # print(splitter[1]) #Zahl
            # print(splitter[2]) #name (nicht immer)

            if not splitter[0] in mainElement:
                if splitter.__len__() > 2:
                    mainElement[splitter[0]] = [{}]
                    aktuelleListe=0
                    anzahlElemente=0

                else:
                    mainElement[splitter[0]] = []

            if splitter.__len__() == 2:
                mainElement[splitter[0]].append(csvFile[key])

            # e.g.: ressourceName_1_name
            if splitter.__len__() == 3:
                if anzahlElemente == 3:
                    anzahlElemente = 0
                    aktuelleListe += 1
                    mainElement[splitter[0]].append({})
                if not mainElement[splitter[0]][aktuelleListe]:
                    mainElement[splitter[0]][aktuelleListe] = {splitter[2]: csvFile[key]}
                    anzahlElemente += 1
                else:
                    mainElement[splitter[0]][aktuelleListe].update({splitter[2]: csvFile[key]})
                    anzahlElemente += 1
        else:
            mainElement.update({key: csvFile[key]})


    mainElement["resourceName"][0]["name"] = str(mainElement["resourceName"][0]["name"]).replace("/", ",")
    filename = pathOutput + str(mainElement["resourceName"][0]["name"]) + '.json'
    # Json Objekt schreiben
    with open(filename, 'w') as jsonFile:
        jsonFile.write(json.dumps(mainElement))


def gui():
    root = tk.Tk()
    root.title('CSV to Json converter')
    root.resizable(False, False)
    root.geometry('300x150')

    def select_file():
        filetypes = (
            ('csv files', '*.csv'),
        )

        filePathCSV = fd.askopenfilename(
            title='Open the .csv File you wan\'t to convert',
            initialdir='/',
            filetypes=filetypes)

        filePathOut = fd.askdirectory(
            title='Open the folder where the json files should be stored.',
            initialdir='/',
        )


        startConversion_Button = ttk.Button(
            root,
            text='start conversion',
            command=conversionManager(filePathCSV, filePathOut + str('/'))
        )

    # open button
    open_button = ttk.Button(
        root,
        text='Open a File',
        command=select_file
    )

    open_button.pack(expand=True)

    # run the application
    root.mainloop()


if __name__ == '__main__':
    gui()
