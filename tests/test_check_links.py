'''
# go through all link and nodes files in hmc_wide_dump
# and check if they are valid with repsect to the given schema
# also check if all links are valid, i.e that we only have allowed links
'''
import os
import pytest
import json


# TODO this should be done better, get from somewhere.
# Currently links do not have some schema
link_specification_dict = {
    'isPartOf':  [('organization','organization')],
    "isPartOf":  [('organization','organization')],
    "conformsTo":  [('organization','organization')],
    "isFormatOf":  [('organization','organization')],
    "hasFormat":  [('organization','organization')],
    "isVersionOf":  [('organization','organization')],
    "hasVersion":  [('organization','organization')],
    "replaces":  [('organization','organization')],
    "isReplacedBy":  [('organization','organization')],
    "references":  [('organization','organization')],
    "isReferencedBy":  [('organization','organization')],
    "requires":  [('organization','organization')],
    "isRequiredBy":  [('organization','organization')],
    "relation": [('software', 'organization')]
}
allowed_relationtype = ['describes',
'isVersionOf',
'isPartOf',
'isIdenticalTo',
'requires',
'isRequiredBy',
'relation',
'conformsTo',
'replaces',
'uses',
'recommends',
'supports',
'provides',
'maintainedBy'
'manufacturedBy',
'issuedBy',
'publishedBy'] # TODO get from somewhere

# Collect the node files
FILE_PATH1 = '../hmc_wide/nodes/'
FILE_PATH2 = '../hmc_wide/links/'
FILE_PATH3 = '../hmc_wide/tables/'
NODES_TABLE_NAME = 'nodes.csv'
LINKS_TABLE_NAME = 'links.csv'

this_file = os.path.dirname(os.path.abspath(__file__))
nodefilefolder = os.path.abspath(os.path.join(this_file, FILE_PATH1))
linkfilefolder = os.path.abspath(os.path.join(this_file, FILE_PATH2))
tablesfilefolder = os.path.abspath(os.path.join(this_file, FILE_PATH3))
link_tablefile = os.path.abspath(os.path.join(tablesfilefolder, LINKS_TABLE_NAME))
node_tablefile = os.path.abspath(os.path.join(tablesfilefolder, NODES_TABLE_NAME))

nodejsonfilelist = []
nodes_uuids = []
for subdir, dirs, files in os.walk(nodefilefolder):
    for file_ in files:
        if file_.endswith('.json'):
            nodejsonfilelist.append(os.path.join(subdir, file_))
            nodes_uuids.append(file_.split('.json')[0])

linkjsonfilelist = []
links_uuids = []
for subdir, dirs, files in os.walk(linkfilefolder):
    for file_ in files:
        if file_.endswith('.json'):
            linkjsonfilelist.append(os.path.join(subdir, file_))
            links_uuids.append(file_.split('.json')[0])

# Since the file system does not allow for two files to have the same name,
# we do not have to check uniqueness. But it could be that someone overrides and
# existing file with something else. This has to be caught in the link checks



@pytest.mark.parametrize('linkfile', linkjsonfilelist)
def test_if_links_validate(linkfile):
    """
    Test if all links json files within the nodefile list validate
    with the corresponding CCT1 schema
    """

    # read
    with open(linkfile, 'r', encoding='utf-8') as fileo:
        data = json.load(fileo)

    # check if keys: 'id_s', 'id_t', category_s, category_t, name_s, name_t, type'
    keys_must = ['id_s', 'id_t', 'relationType', 'category_s', 'category_t']#, 'name_s', 'name_t']
    for key in keys_must:
        assert key in data.keys()

    # check if link type exists
    assert data['relationType'] in allowed_relationtype

    # check if the categories are according to the type.
    link_specification = link_specification_dict[data['relationType']]
    assert (data['category_s'], data['category_t']) in link_specification

    # check if id_s and id_t exist in node list
    assert data['id_s'].replace('/', '.') in nodes_uuids
    assert data['id_t'].replace('/', '.') in nodes_uuids

    # read id_s and id_t files and check if names and categories are correct
    # since we enforce a certain link
    assert data['category_s'] in data['id_s']
    assert data['category_t'] in data['id_t']
