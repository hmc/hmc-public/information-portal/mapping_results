'''
# go through all link and nodes files in hmc_wide_dump
# and check if they are valid with repsect to the given schema
# also check if all links are valid, i.e that we only have allowed links
'''
import os
import pytest
import json
import subprocess
from jsonschema import validate
from jsonschema.exceptions import ValidationError
from hmc_schemas import load_schema
from pathlib import Path

# Collect the node files
FILE_PATH1 = '../hmc_wide/'
FILE_PATH3 = '../hmc_wide/tables/'
NODES_TABLE_NAME = 'nodes.csv'

this_file = os.path.dirname(os.path.abspath(__file__))
nodefilefolder = os.path.abspath(os.path.join(this_file, FILE_PATH1))
tablesfilefolder = os.path.abspath(os.path.join(this_file, FILE_PATH3))
node_tablefile = os.path.abspath(os.path.join(tablesfilefolder, NODES_TABLE_NAME))

nodejsonfilelist = []
nodes_uuids = []
for subdir, dirs, files in os.walk(nodefilefolder):
    for file_ in files:
        if file_.endswith('.json'):
            nodejsonfilelist.append(os.path.join(subdir, file_))
            nodes_uuids.append(file_.split('.json')[0])


folder_structure = {
        '10_policies': 'policy',
        '1_repositories' : 'repository',
        '5_data_sources' : 'data_source',
        '9_organizations' : 'organization',
        '11_documents': 'document',
        '2_metadata_standards': 'metadata_standard',
        '6_identifiers' : 'pid_system',
        '12_schema_crosswalks' : 'crosswalk',
        '3_terminologies' : 'terminology',
        '7_data_formats' : 'data_format',
        '13_methods' : 'method',
        '4_software' : 'software',
        '8_licenses' : 'license'
        }

cats = list(folder_structure.keys())
cats.sort()

changed_file_list = []
fail = False
# get the files which changed in last commit over git
try:
    res = subprocess.run(["git", "diff" ,"--name-only", "HEAD", "HEAD~1"], capture_output=True, text=True)
except Exception as err:
    print(str(err))
    fail = True
    raise err
if not fail:
    for path in res.stdout.split('\n'):
        if path.endswith('.json'):
            path = Path('..').resolve() / path
            if path.exists(): # could be deleted in last commit
                changed_file_list.append(path)

print(changed_file_list)
# maybe change this to get a test for each category

error_stats = {cat:{} for cat in folder_structure.values()}

# Since the file system does not allow for two files to have the same name,
# we do not have to check uniqueness. But it could be that someone overrides and
# existing file with something else. This has to be caught in the link checks

#@pytest.print_stats(error_stats)
@pytest.mark.slow
@pytest.mark.parametrize('nodefile', nodejsonfilelist)
def test_if_nodes_validate(nodefile):
    """
    Test if all nodes json files within the nodefile list validate
    with the corresponding CCT1 schema
    """

    validate_given_file(nodefile)

@pytest.mark.fast
@pytest.mark.parametrize('changed_file', changed_file_list)
def test_if_changed_files_validate(changed_file):
    """
    Test if all json files within the last commit validate
    with the corresponding CCT1 schema
    """

    validate_given_file(changed_file)


def validate_given_file(nodefile):
    """
    Functions used in the tests
    """
    with open(nodefile, 'r', encoding='utf-8') as fileo:
        data = json.load(fileo)

    # find out type
    # the type can be found throuch the folder structure
    cat_type = data.get('_type', data.get('mappingCategory', None))
    if cat_type is None:
        for key, val in folder_structure.items():
            if key in str(nodefile):
                cat_type = val
    if cat_type is None:
        id_ = data.get('_id', None)
        if id_ is None:
            raise ValueError('Could not find out Category of the node, please specify in "_type" key and or structure of "_id" key.')
        #nodefile.replace('.','/')
        #assert id_ == nodefile
        cat_type = id_.split('/')[0].split('_')[-1]

    # get right schema
    schema = load_schema(cat_type)

    # ignore all keys with '_' for validation
    datat = {}
    for key, val in data.items():
        if not key.startswith('_'):
            datat[key] = val

    # validate, but catch exception to add to statistics
    try:
       validate(instance=datat, schema=schema) # Will throw errors if not
    except Exception as ex:
        # count and reraise
        stat = error_stats[cat_type].get(ex, 0)
        stat = stat + 1
        error_stats[cat_type][ex] = stat
        raise ex

print(error_stats) # Does not work that way
