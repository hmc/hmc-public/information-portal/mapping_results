from jsonschema import validate
# from Validator import validate

schema = {
    "type": "object",
    "properties": {
        "name": {"type": "string"},
        "age": {"type": "number"},
    },
    "required": ["name"],
    "additionalProperties": False
}
try:
    validate(instance={"name": "John", "age": 30, "job": "Engineer"}, schema=schema)

except Exception as exp:
    # Ak codes here
    # the final print for displaying error_stats were not working as expected
    print(exp)

############################################################################################

import jsonschema
from jsonschema.exceptions import ValidationError, SchemaError
import json

# Your JSON data
json_data = {
    "name": "John",
    "age": "30",  # Should be an integer
    # Other properties...
}

# Your JSON schema
json_schema = {
    "type": "object",
    "properties": {
        "name": {"type": "string"},
        "age": {"type": "integer"},
        # Other properties...
    },
    "required": ["name", "age"],
    "additionalProperties": False,  # No additional properties allowed
    # Other schema rules...
}

# try:
#     # 1. Syntax Errors: Invalid JSON
#     json_data_str = '{"name": "John", "age": 30,}'  # Trailing comma is invalid
#     parsed_data = json.loads(json_data_str)
#     jsonschema.validate(instance=parsed_data, schema=json_schema)
#
# except json.JSONDecodeError as e:
#     print(f"Syntax Error: {e}")

# try:
#     # 2. Schema Validation Errors: Missing 'age' property
#     json_data_missing_age = {"name": "John"}
#     jsonschema.validate(instance=json_data_missing_age, schema=json_schema)
#
# except ValidationError as e:
#     print(f"Schema Validation Error: {e.message}")
#
# try:
#     # 3. Additional Properties Errors: Additional property 'email'
#     json_data_additional_props = {"name": "John", "age": 30, "email": "john@example.com"}
#     jsonschema.validate(instance=json_data_additional_props, schema=json_schema)
#
# except ValidationError as e:
#     print(f"Additional Properties Error: {e.message}")
#
# try:
#     # 4. Array Validation Errors: Invalid item type in array
#     json_data_invalid_array = {"name": "John", "age": 30, "hobbies": ["swimming", 42]}
#     json_schema_with_array = {
#         "type": "object",
#         "properties": {
#             "name": {"type": "string"},
#             "age": {"type": "integer"},
#             "hobbies": {"type": "array", "items": {"type": "string"}}
#         },
#         "required": ["name", "age"],
#     }
#     jsonschema.validate(instance=json_data_invalid_array, schema=json_schema_with_array)
#
# except ValidationError as e:
#     print(f"Array Validation Error: {e.message}")
#
# try:
#     # 5. Dependency Errors: 'email' depends on 'name'
#     json_schema_with_dependency = {
#         "type": "object",
#         "properties": {
#             "name": {"type": "string"},
#             "email": {"type": "string"}
#         },
#         "required": ["name"],
#         "dependencies": {"email": ["name"]}
#     }
#     json_data_dependency_error = {"email": "john@example.com"}
#     jsonschema.validate(instance=json_data_dependency_error, schema=json_schema_with_dependency)
#
# except ValidationError as e:
#     print(f"Dependency Error: {e.message}")
#
# try:
#     # 6. Format Errors: Invalid email format
#     json_schema_with_email_format = {
#         "type": "object",
#         "properties": {
#             "email": {"type": "string", "pattern": "([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z]{2,})+", "format": "email"}
#         }
#     }
#     json_data_invalid_email = {"email": "invalid@email@a.la"}  # Invalid email format
#     jsonschema.validate(instance=json_data_invalid_email, schema=json_schema_with_email_format)
#
# except ValidationError as e:
#     print(f"Format Error: {e.message}")
#
try:
    # 7. Reference Errors: Using an undefined reference
    json_schema_with_reference_error = {
        "$ref": "#/definitions/person",
        "definitions": {
            "person": {
                "type": "object",
                "properties": {
                    "name": {"type": "string"}
                }
            }
        }
    }
    json_data_reference_error = {"name": "John"}
    jsonschema.validate(instance=json_data_reference_error, schema=json_schema_with_reference_error)

except jsonschema.exceptions.RefResolutionError as e:
    print(f"Reference Error: {e}")
#
# try:
#     # 8. Circular Reference Errors: Circular reference in schema
#     json_schema_with_circular_reference = {
#         "$ref": "#/definitions/person",
#         "definitions": {
#             "person": {
#                 "type": "object",
#                 "properties": {
#                     "name": {"type": "string"},
#                     "spouse": {"$ref": "#/definitions/person"}  # Circular reference
#                 }
#             }
#         }
#     }
#     json_data_circular_reference_error = {"name": "John", "spouse": {"name": "Alice"}}
#     jsonschema.validate(instance=json_data_circular_reference_error, schema=json_schema_with_circular_reference)
#
# except jsonschema.exceptions.RefResolutionError as e:
#     print(f"Circular Reference Error: {e}")
#
# try:
#     # 9. Custom Validation Errors: Custom validation with a function
#     def custom_validator(instance, schema):
#         if "custom_property" not in instance:
#             raise ValidationError("Custom property 'custom_property' is missing.")
#
#     json_schema_with_custom_validation = {
#         "type": "object",
#         "properties": {
#             "custom_property": {"type": "string"}
#         }
#     }
#     json_data_custom_validation_error = {"name": "John"}
#     jsonschema.validate(instance=json_data_custom_validation_error, schema=json_schema_with_custom_validation, validators=[custom_validator])
#
# except ValidationError as e:
#     print(f"Custom Validation Error: {e.message}")
#
# try:
#     # 10. Schema Load Errors: Invalid schema file
#     invalid_schema = {
#         "typed": "object",
#         "properties": {
#             "name": {"string"}
#         }
#     }
#     json_data_valid = {"name": "John"}
#     jsonschema.validate(instance=json_data_valid, schema=invalid_schema)
#
# except SchemaError as e:
#     print(f"Schema Load Error: {e.message}")

except jsonschema.exceptions.RefResolutionError as e:
    print(f"Reference Error2: {e}")
except json.JSONDecodeError as e:
    print(f"Syntax Error: {e}")
except Exception as e:
    print(f"An unexpected error occurred: {str(e)}")

else:
    print("JSON data is valid according to the schema.")
