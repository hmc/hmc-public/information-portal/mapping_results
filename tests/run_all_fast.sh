#!/usr/bin/env sh
pytest --junitxml=coverage.xml --cov=../util/ --cov=./ -v -m "not slow"
