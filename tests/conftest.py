# -*- coding: utf-8 -*-
"""
Configurations and fixtures for mapping_results tests
"""
import pytest


def pytest_configure(config):
    config.addinivalue_line("markers", "slow : mark slow test to allow to run them sperately")
    config.addinivalue_line("markers", "fast : mark fast test to allow to run them sperately")

