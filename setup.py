# -*- coding: utf-8 -*-
"""
setup: usage: pip install -e .
"""

from __future__ import absolute_import
from setuptools import setup, find_packages
import io  # needed to have `open` with encoding option

# read the contents of your README file
from os import path

this_directory = path.abspath(path.dirname(__file__))
with io.open(path.join(this_directory, 'README.md'), encoding='utf8') as f:
    long_description = f.read()

if __name__ == '__main__':
    setup(
        name='mapping_results',
        version='0.1.0',
        description='Data from the mapping with tools and test to check consistency with HMC schemas the context of the Helmholtz metadata collaboration (HMC).',
        # add long_description from readme.md:
        long_description = long_description, # add contents of README.md
        long_description_content_type ='text/markdown',  # This is important to activate markdown!
        url='https://gitlab.hzdr.de/hmc/hmc/cct-1-mapping/mapping_results',
        author='HMC',
        author_email='HMC@fz-juelich.de',
        license='MIT License, see LICENSE.txt file.',
        classifiers=[
            'Development Status :: 1 - Alpha',
            'Intended Audience :: Science/Research',
            'License :: OSI Approved :: MIT License',
            'Programming Language :: Python :: 3',
            'Programming Language :: Python :: 3.7',
            'Programming Language :: Python :: 3.8',
            'Programming Language :: Python :: 3.9',
        ],
        keywords='metadata, data mining, data publications',
        packages=find_packages(exclude=['tests*']),
        include_package_data=True,

        install_requires=[
            'jsonschema',
            'pandas',
        # 'hmc-schemas' needed, but CI does not reconize it if it is installed... and tries to install it again. once public this is easy
        #    'hmc-schemas @ git+ssh://git@codebase.helmholtz.cloud/hmc/hmc/cct-1-mapping/category-definitions.git#egg=category-definitions-0.1.0'
        ],
        extras_require={
            'testing': [
                'pytest-cov >= 2.5.0',
                'pytest'],
            'pre-commit': [
                'pre-commit',
            ]
        },
    )
